/* Quiz 1:
1. What is the term given to unorganized code that's very hard to work with?
2. How are object literals written in JS?
3. What do you call the concept of organizing information and functionality to belong to an object?4. If the studentOne object has a method named enroll(), how would you invoke it?
5. True or False: Objects can have objects as properties.
6. What is the syntax in creating key-value pairs?
7. True or False: A method can have no parameters and still work.
8. True or False: Arrays can have objects as elements.
9. True or False: Arrays are objects.
10. True or False: Objects can have arrays as properties. 

/* Answer

1. spaghetti code
2. using curly braces {}
3. Encapsulation
4. studentOne.enroll();
5. True
6. key: value
7. True
8. True
9. True
10. True
*/ 

// Function Coding
function Student(name, email, grades) {
    this.name = name;
    this.email = email;
    this.grades = grades;

    this.login = function () {
        console.log(`${this.email} has logged in`);
    };

    this.logout = function () {
        console.log(`${this.email} has logged out`);
    };

    this.listGrades = function () {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    };

    this.computeAve = function () {
        return this.grades.reduce((sum, grade) => sum + grade, 0) / this.grades.length;
    };

    this.willPass = function () {
        return this.computeAve() >= 85;
    };

    this.willPassWithHonors = function () {
        const average = this.computeAve();
        if (average >= 90) return true;
        if (average >= 85) return false;
        return undefined;
    };
}

const studentOne = new Student('John', 'john@mail.com', [90, 88, 92, 86]);
const studentTwo = new Student('Joe', 'joe@mail.com', [78, 85, 88, 90]);
const studentThree = new Student('Jane', 'jane@mail.com', [95, 92, 89, 87]);
const studentFour = new Student('Jessie', 'jessie@mail.com', [80, 82, 78, 88]);

const classOf1A = {
    students: [studentOne, studentTwo, studentThree, studentFour],

    countHonorStudents() {
        return this.students.filter(student => student.willPassWithHonors()).length;
    },

    honorsPercentage() {
        return (this.countHonorStudents() / this.students.length) * 100;
    },

    retrieveHonorStudentInfo() {
        return this.students.filter(student => student.willPassWithHonors())
            .map(student => ({ email: student.email, aveGrade: student.computeAve() }));
    },

    sortHonorStudentsByGradeDesc() {
        return this.retrieveHonorStudentInfo().sort((studentA, studentB) => studentB.aveGrade - studentA.aveGrade);
    }
};



